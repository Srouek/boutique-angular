import { Component, OnInit } from '@angular/core';
import { PanierService } from '../../services/panier.service';
import { PanierLigne } from '../../entities/panierLigne';
import { Panier } from '../../entities/panier';


@Component({
  selector: 'app-Panier',
  templateUrl: './Panier.component.html',
  styleUrls: ['./Panier.component.css']
})
export class PanierComponent implements OnInit {
title = "Mon Panier d'article";
  panier : Panier;

  constructor(private panierService : PanierService,
    public panierSVC : PanierService) { }

  ngOnInit() {
    this.panier = this.panierService.panier;
   

  }

}
