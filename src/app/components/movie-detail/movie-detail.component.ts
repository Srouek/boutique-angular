import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/entities/movie';
import { MovieService } from 'src/app/services/movie.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  movie:Movie;
  genres: string[];


  constructor(private movieSVC : MovieService,
              private route : ActivatedRoute) { }


  genre(){
    this.genres = this.movie.Genre.split(',') ;
    return this.genres;
  }

  ngOnInit() {
    let id =  this.route.snapshot.paramMap.get('idMovie');

    this.movieSVC.getMovieById(id).subscribe(m => this.movie = m);
  }

}
