import { Component, OnInit } from '@angular/core';
import { Article } from '../../entities/article';
import { CatalogueService } from '../../services/catalogue.service';
import { PanierService } from 'src/app/services/panier.service';

@Component({
	selector: 'app-catalogue',
	templateUrl: './catalogue.component.html',
	styleUrls: [ './catalogue.component.css' ]
})
export class CatalogueComponent implements OnInit {
	catalogue: Article[];
	selectedArticle: Article;

	constructor(private catalogueService: CatalogueService, public panierSVC: PanierService) {}

	ngOnInit() {
		this.catalogueService.getArticles().subscribe((result) => (this.catalogue = result));
	}

	choisir(article: Article): void {
		this.selectedArticle = article;
	}
}
