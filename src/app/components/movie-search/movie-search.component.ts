import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/entities/movie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {

  movies: Movie[];
  titre : string;

  constructor(private movieSCV : MovieService) { }

  search(){
    this.movieSCV.searchMovies(this.titre).subscribe((data:any) => {this.movies = data.Search;} );
    
  }

  ngOnInit() {
  }

}
