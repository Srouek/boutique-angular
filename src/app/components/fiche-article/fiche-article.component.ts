import { Component, OnInit } from '@angular/core';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/entities/article';

@Component({
  selector: 'app-fiche-article',
  templateUrl: './fiche-article.component.html',
  styleUrls: ['./fiche-article.component.css']
})
export class FicheArticleComponent implements OnInit {


  article : Article;
  id: number = 1;

  constructor(private articleSVC : CatalogueService,
              private route : ActivatedRoute) { }

  ngOnInit() {

    let id = Number (this.route.snapshot.paramMap.get('idArticle'));
    // Récupérer l'objet article au preès du service 
    this.articleSVC.getArticleById(id).subscribe(d => this.article = d);
  }

}
