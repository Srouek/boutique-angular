import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Article } from '../../entities/article';
import { PanierService } from '../../services/panier.service';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit, OnChanges {

  @Input() article : Article;
  quantite : number = 0;

  constructor(private panierSVC : PanierService) { }

  ngOnChanges(){
    this.quantite = 0;
  }

  ajout(){
    this.panierSVC.ajouter(this.quantite, this.article);
  }

  ngOnInit() {
  }

}
