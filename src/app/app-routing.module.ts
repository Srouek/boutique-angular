import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { PanierComponent } from './components/Panier/Panier.component';
import { FicheArticleComponent } from './components/fiche-article/fiche-article.component';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';

const routes: Routes = [
    {path:'articles', component:CatalogueComponent},
    {path:'panier', component:PanierComponent},
    {path:'articles/:idArticle',component:FicheArticleComponent},
    {path:'movies',component:MovieSearchComponent},
    {path:'movies/:idMovie', component:MovieDetailComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{}