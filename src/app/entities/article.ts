export interface Article {

    id: number;
    libelle: string;
    pu : number;
    description: string;
    promo : number;
}
