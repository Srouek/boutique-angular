import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PanierLigne } from '../entities/panierLigne';
import { Article } from '../entities/article';
import { Panier } from '../entities/panier';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  
  panier : Panier = new Panier();
  
  
  constructor() { }

  ajouter(qte:number, article: Article) : void {

    for (const ligne of this.panier.lignes) {
      if(ligne.article === article){
        ligne.quantite += qte;
        return;
      }
    }
    this.panier.lignes.push({quantite:qte, article:article});
  }

 

  getQuantiteTotal(): number{
    let result = 0;
    for (const ligne of this.panier.lignes) {
      result += ligne.quantite;
    }
    return result;
  }
  

  
}
