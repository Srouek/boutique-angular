import { Injectable } from '@angular/core';
import { Article } from '../entities/article';
import { CATALOGUE } from '../mocks/mock-catalogue';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  getArticles() : Observable<Article[]>{
    return of(CATALOGUE);
  }

  getArticleById(id:number) : Observable<Article>{
    let result : Article;

    for (const article of CATALOGUE) {
      if(article.id == id){
        result = article;
      }
    }

    return of(result);
  }

constructor() { }

}
