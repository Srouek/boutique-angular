import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../entities/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  provider : string = "http://www.omdbapi.com/";
  key: string = "1d4bb3ab";

  

constructor(private http : HttpClient) { }

getMovie(title: string) : Observable<Movie>{

  let url = `${this.provider}?t=${title}&apikey=${this.key}`;
  return this.http.get<Movie>(url);
}

searchMovies(title:string){

  let url = `${this.provider}?s=${title}&apikey=${this.key}`;
  console.log(url);
  return this.http.get(url);
}

getMovieById(id:string) : Observable<Movie>{
  let url = `${this.provider}?i=${id}&apikey=${this.key}`;
  return this.http.get<Movie>(url);
}


}
