import { Article } from '../entities/article';

export const CATALOGUE : Article[] = [
    {id : 1, libelle: "Coupe choux", pu : 17.34, description : "La star des rasoirs manuels est clairement le coupe choux, qu’on appelle aussi « Straight Razor » en anglais. Largement mis en avant chez les barbiers, c’est « l’arme fatale » que tout les gentlemen rêvent secrètement de manier…", promo : 0.3},
    {id : 2, libelle: "Brosse a Barbe", pu : 15, description : "Un must-have dans le kit de toilettage des hommes barbus. La brosse à barbe, fabriquée à partir de poils 100% naturels de sanglier, vous offre une finition parfaite qui ne peut être obtenue avec n’importe quelle brosse.",promo :0},
    {id : 3, libelle: "Ciseaux a barbe", pu : 22.99, description : "L’outil indispensable pour l’entretien de votre barbe- Ces ciseaux à barbe sont excellents pour couper votre barbe avec la plus grande précision. Ils sont légers et polyvalents. Les ciseaux BARBER TOOLS vous permettent d’obtenir la coupe parfaite, qu’il s’agisse de votre barbe ou de votre moustache.", promo : 0.2},
  ]